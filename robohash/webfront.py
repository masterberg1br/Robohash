from __future__ import unicode_literals
import tornado.httpserver
import tornado.ioloop
from tornado.options import define, options
import tornado.web
import socket
import os
from robohash import Robohash
import urllib.request
import urllib.parse

urlopen = urllib.request.urlopen
urlencode = urllib.parse.urlencode


define("port", default=80, help="run on the given port", type=int)


class ImgHandler(tornado.web.RequestHandler):
    """
    The ImageHandler is our tornado class for creating a robot.

    called as Robohash.org/$1, where $1 becomes the seed string
    for the Robohash obj
    """

    def get(self, string=None):

        # Set default values
        size = 300
        color = None

        args = self.request.arguments.copy()

        for k in list(args.keys()):
            v = args[k]
            if isinstance(v) is list:
                if len(v) > 0:
                    args[k] = args[k][0]
                else:
                    args[k] = ""

        # Ensure we have something to hash!
        if string is None:
            string = self.request.remote_ip

        # Split the size variable in to sizex and sizey
        if "size" in args:
            size = args["size"]
            if size > 4096 or size < 0:
                size = 300

        # Create our Robohashing object
        r = Robohash(string)

        # Allow users to manually specify a robot 'set' that they like.
        # Ensure that this is one of the allowed choices, or allow all
        # If they don't set one, take the first entry from sets above.

        if args.get("set", r.sets[0]) in r.sets:
            roboset = args.get("set", r.sets[0])
        elif args.get("set", r.sets[0]) == "any":
            # Add ugly hack.

            # Adding cats and people per submitted/requested code, but I don't
            # want to change existing hashes for set=any
            # so we'll ignore those sets for the 'any' config.
            roboset = r.sets[r.hasharray[1] % (len(r.sets) - 2)]
        else:
            roboset = r.sets[0]

        # If they specified multiple sets, use up a bit of randomness to
        # choose one.
        # If they didn't specify one, default to whatever we decided above.

        possiblesets = []
        for tmpset in args.get("sets", roboset).split(","):
            if tmpset in r.sets:
                possiblesets.append(tmpset)
        if possiblesets:
            roboset = possiblesets[r.hasharray[1] % len(possiblesets)]

        # If they DID choose set1, randomly choose a color.
        if roboset == "set1" and color is None:
            color = r.colors[r.hasharray[0] % len(r.colors)]
            roboset = "set1"

        # We're going to be returning the image directly, so tell the browser
        # to expect a binary.
        self.set_header("Content-Type", "image/png")
        self.set_header("Cache-Control", "public,max-age=31536000")

        # Build our Robot.
        r.assemble(roboset=roboset, color=color, size=size)

        # Print the Robot to the handler, as a file-like obj
        r.img.save(self, format="png")


def main():
    tornado.options.parse_command_line()
    # timeout in seconds
    timeout = 10
    socket.setdefaulttimeout(timeout)

    settings = {
        "static_path": os.path.join(os.path.dirname(__file__), "static"),
        "cookie_secret": "9b90a85cfe46cad5ec136ee44a3fa332",
        "login_url": "/login",
        "xsrf_cookies": True,
    }

    application = tornado.web.Application([(r"/(.*)", ImgHandler)], **settings)

    http_server = tornado.httpserver.HTTPServer(application, xheaders=True)
    http_server.listen(options.port)

    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
